layout: default.liquid
---
### Index:

{% for post in collections.posts.pages %}
{{post.published_date | date: "%b %d, %Y"}} - [{{ post.title }}]({{ post.permalink }})
{% endfor %}


###### Built with ❤️️  using [Cobalt](https://cobalt-org.github.io/). A straightforward static site generator (SSG) written in [Rust](https://www.rust-lang.org/).

---
| [Home](/blog/) | [About](about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) | [RSS]() <a href="https://validator.w3.org/feed/check.cgi?url=https%3A//thedavidross.gitlab.io/blog/rss.xml"><img src="/blog/img/valid-rss-rogers.png" alt="[Valid RSS]" title="Validate my RSS feed" /></a>
