title: Fitness Robots of Beige
published_date: "2016-11-21 14:03:34 +0000"
layout: default.liquid
is_draft: false
---
# Fitness Robots of Beige

Social media is awash with disingenuous activity. From fake news, fake users, robot account farms, to automated spam marketing to buy 10,000 fake followers. It's hard for the uninitiated to know what's real any more.

<blockquote class="twitter-tweet" data-lang="en-gb"><p lang="en" dir="ltr">A world with or without fact-checking? Think before you share! <a href="https://t.co/RwddHpJdIA">pic.twitter.com/RwddHpJdIA</a></p>&mdash; Viralgranskaren (@viralgranskaren) <a href="https://twitter.com/viralgranskaren/status/799621952071696385?ref_src=twsrc%5Etfw">18 November 2016</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

The fitness industry can often be no better.

It's a fine line between what is fake and what is a fad. This industry is fast to be enthralled by the latest fad bucks and quick to move onto the next. So who do we trust? Who is an [independent expert](http://doctoraseem.com/), and who is [in the pay-pocket of the industrialised food complex](http://www.foodpolitics.com/2013/01/new-study-big-foods-ties-to-registered-dietitians/) responsible for getting our nations into the saggy-boned state we're in?

We are all of the one species, [Homo Sapiens](https://en.wikipedia.org/wiki/Homo_sapiens), but through our wide variety of interests, desires, motivations, and even biological differences, it's hard to imagine that an annoying algorithmically designed push notification from your wearable device to your mobile is really the best that we can get. Is artificial intelligence even the optimal way forward for fitness?

[This recent article](https://www.theguardian.com/careers/2016/may/11/robot-jobs-automated-work) in the Guardian talks of social and physical activity being an area that will likely not be at risk from automation due to the complex dynamic ways in which we freely move and engage through our exercise lifestyles. And yet the industry has been focusing on [cheaper gym memberships](http://www.fool.co.uk/investing/2016/10/25/2-leisure-stocks-i-expect-to-soar-in-brexit-britain/) often [at the detriment of a skilled professional workforce](http://ehstoday.com/health/workplace-health-and-wellness-drops-fitness-trend-list). It's a highly recognised statistic that 80% of paying gym members do not even turn up, and it's even a long standing industry business model.

[Recent economic](https://www.theguardian.com/commentisfree/2016/oct/11/the-guardian-view-on-the-pounds-crash-the-markets-deliver-their-verdict-on-brexit-britain-and-its-not-pretty) scenarios [across the globe](http://www.straitstimes.com/asia/south-asia/indias-cash-flow-crisis-6-other-countries-that-experienced-distress-over-currency) have shown just how vulnerable we all are to things that may feel out of our control. The explosion in popularity of wearable devices (in contrast to their limited use case experienced by [many soon shelving them](https://www.bloomberg.com/view/articles/2016-03-03/why-nobody-s-wearing-wearables)) and new ways of working in the sharing economy (in contrast to the costs of [greater loss of individual freedom](http://www.nytimes.com/2014/08/17/technology/in-the-sharing-economy-workers-find-both-freedom-and-uncertainty.html) and for many the explosion of [self-employment introducing the real struggle](https://www.thersa.org/discover/publications-and-articles/rsa-blogs/2015/03/8-key-take-aways-from-our-new-report-on-the-living-standards-of-the-self-employed) simply in surviving) could be noted.

I trained as a Personal Trainer with Premier International, and even though I paid the equally 'premier' fees for the privilege of the certification, succumbed to the burn & churn of the transient nature in the industry. There's never a shortage of fit, nubile, new blood to fill the gaps when the self-employed trainer, suffering through extortionate facility hiring fees, might find it harder than they ever imagined. Usually with the added restriction of being unable to even promote the fact you're there. Do you even exist? Does your experience come across to your fake Instagram selfie followers?

What we're falling into a new fitness control mechanism that's being created from the top - down. We can choose to participate and lose even more recognition and basic human rights. If not, what alternatives are there?

I'm propose a new fitness paradigm that will empower fitness experts. Bring in new leads via a platform that will integrate with the modern world. Not lock you into a traditional gym subscription model.

At it's core:
1) Respect of Privacy - by default! No harvesting of data to sell a digital version of you to the highest digital marketing bidder.
2) Connecting real people to real results. Real biodynamics beats automation. Every. Single. Time.
3) Platform agnostic. Locking you down to a particular device, or multinational chain of gym, entirely sucks.
4) Geographically diverse. Flexibility to move alongside your life.
5) Open source. The ability to integrate into your business life, the way you want it to.
If this concept has grabbed your attention, and you want to help me create this community vision, then get in contact.

Thanks for reading! Don't forget to breathe.

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
