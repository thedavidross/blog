title: "MozFest is for All of Us"
published_date: "2018-07-11 16:57:37 +0000"
layout: default.liquid
is_draft: false
---
# MozFest is for all of us!

#### Data and You

Yes - YOU! Hello, you. 👋 😁

2018's Mozilla Festival, also known as MozFest, is just around the corner. Our theme this year is "Data and You".

Over the last year you've likely seen more about "Privacy" and "Security" than in most of your lifetime. From implications of potentially disrupting the very democratic process, your email inbox sent into overdrive via the European Union's launch of the General Data Protection Regulation (GDPR), to the latest DDoS security threat against your favourite website and personal data. You won't have missed issues like these, but have we and our friends really changed our habits? What could we, as a community, be doing instead?

#### Who

All kinds of people come to visit Mozfest. From students and academia, to journalists, technologists, artists, entrepreneurs, civil servants, librarians, publishers, designers, activists, funders, politicians, volunteers, staff, standards bodies, children, world experts and fascinated newbies alike. Hopefully you too?

<img src="/blog/img/paolo.jpg">

###### Image CC-BY-2.0 [Mozilla Festival](https://www.flickr.com/photos/mozfest/24169852968/) - Privacy & Security 2017 Artist, Paolo Cirio

#### What

The [Mozilla Festival](https://mozillafestival.org/about) (aka #Mozfest) is the 9th annual global gathering - held over 9 floors of Ravensbourne College, Greenwich Peninsula, London.

#### When

We've already opened the [Call for Proposals](https://mozillafestival.org/proposals) but the closing date is August 1. You've still got time to apply. We've even set up a [channel to answer your session proposal questions](https://gitter.im/mozilla/mozfest-cfp-help). All through August content wranglers, just like me, will be reading these proposals and building a collection of our community to hold space 26–29 October. The excitement is already growing, day by day!

#### Where

I'm on the small team representing the [Privacy and Security space](https://mozillafestival.org/spaces#Privacy-and-Security). We recognise its impact on an personal level, and also the transpersonal, at a global scale. It's one of the major topics in Mozilla's interactive, living document, called the [Internet Health Report](https://internethealthreport.org/2018/), where we encourage your voice on any of the published issues. Now that crises like Cambridge Analytica / Facebook has been in the public knowledge for some months, it might be shocking to discover Facebook's user numbers have shot up? After the successes in India against Free Basics it's been disorientating to see the human impact of Adhaar. The EU's GDPR saw all our email inboxes swamped for weeks, but now it's died down a little many still report on a poorer experience or even being blocked from reading content. Is this the best we can do?

**These issues affect us all, no matter where we live in the world.**

#### Why

Each year we gather a global community who are equally passionate about the Open Web. People are its glowing centre. We bring a hugely varied spectrum of voices from across the world. Not your bog-standard, boring old conference where you go to get talked at. Oh, certainly not. Instead it's a fun blend of workshops, roundtable discussions, and interactive works set to bring back the play, a collective sense of wonder, and to inspire conversation among new friends. Diversity at this event is not an afterthought we might shove into short document attempting to say all the correct words. [It's visible through our team](https://mozillafestival.org/team/wranglers)'s always improving diversity, our energies driven towards solving big issues, and to be inspired by the commonality often hidden in each other's everyday experiences. Truly extraordinary stories are often to be discovered in initially ordinary moments.

#### Total chaos

In our search for helpful information, particularly regarding our personal privacy and security, we find ourselves at a crossroads. A path towards what we know and trust, or the path to new information. You scan to the right and you might discover a hidden third way that's not even on the map. This hidden due to what exactly? Your lack of time? Assumption there was only 2 choices? Being encouraged and guided in the opposite direction? The fact *most people* go one of those other ways? Is it the right decision? Do you feel comfortable going at it alone? If you're making the decisions for a business or community the additional pressure's very real! Who designed this map anyway? And who funded it? You might have spent hours researching how to do this a year ago, and now forget it all. ¯\_(ツ)_/¯

#### Problem

It's difficult for us as individuals to understand what actions we can take to positively affect this. Who to trust? How to most effectively help others in our communities and be a guiding light? We're at a time when hypothesising about risks can no longer be seen as "job done". Across the diverse [Mozfest Spaces](https://www.mozillafestival.org/spaces) and experiences, you'll discover these core topics unveiling many challenges, questions, discoveries, and successes in making the Open Web better for all of us:
- Privacy and Security
- Openness
- Digital Inclusion
- Web Literacy
- Decentralization
- Queering Mozfest
- Youth Zone

#### Solution

I don't pretend to have the solutions. We need your help. In our effort to find them our spaces invite you to join us in London in October.

[Send in a proposal](https://mozillafestival.org/proposals), by August 1, and journey to real world solutions right beside our awesome attendees. Collaboratively learning is a powerful tool in this troubled world. Come along and make an impact.

#### Isolation

Ever mentioned your privacy concerns to have a friend say they "have nothing to nothing to hide, so nothing to fear"? I've got to the point of screaming everytime I hear it. Do you sometimes feel you've somehow lost a sense of yourself along the way? It can be exhausting. Recognition that there's a problem, is often a first step to find the solution which fits YOU. Mozfest is jam-packed full of people on this same journey.

#### Community

Come build your community right inside our vibrant community. Be challenged by other viewpoints, bring your perspective, connect in real life.

#### Network

Year after year people connect with the unfamiliar. To explore. Many now work together long after Mozfest on projects inspired by its sessions and sense of spirit to take actions today.

#### Newbie

You might only come across what initially appear as complex issues, surrounding Privacy and Security, when things go wrong. One of your friends might constantly talk about how you should be careful. But what does this even mean? Provoking questions in us can have tremendous value to our learning. Though for some, it can fill them with dread, or even flippant disregard.

#### Intermediate

You might have learned the basics, but now find the next steps are often beyond your reach. Some communities are resistant to helping you out, and might even make you feel unwelcomed. Tell people what you've experienced. Your story is an engaging way to connect with others.

#### Advanced

By learning, often the hard way, how complex these fields are you rapidly appreciate how fast your time can be taken from you when you try to help others. You might have some ideas about helping many users, but getting the message out there can be time consuming. Are you effectively working towards the impact your ideas deserve? Build new networks to collaborate and create new ethical opportunities.

#### Teacher

If you're an advocate or educator you'll immediately recognise the value in 2-way learning. How have you put some strategies to task? What has most surprised you on this path?

#### Learner

As learners, we might never ever cease our learning. Or you might need a simple overview and instead battle through documents, as thick as War and Peace, just in an attempt to work with someone. To find the answer to your burning question. Connecting with real people in a safe environment offline can feel magical.

#### Just join us

Start creating a proposal today by checking out [these ace tips](https://medium.com/mozilla-festival/7-steps-to-a-great-mozfest-proposal-eff4f1cee6fe). Join a truly global network of Mozillians in amplifying this opportunity in your networks. Mention it to that friend. Support each other. We've got a lot wrong with this world, but only working openly, and together, might we bring ourselves closer to fixing it.

[Mozfest is for all of us](https://mozillafestival.org/expect) - We'd love to see you in October.

<img src="/blog/img/mozfestcfp.jpg">

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
