title: "I'm a failure*"
published_date: "2016-11-20 11:21:22 +0000"
layout: default.liquid
is_draft: false
---
# I'm a failure*

Let me set the tone. Back in 2014 I found that I needed to up my game. I was stuck. I felt a failure.

<img src="/blog/img/doingitwrong.jpg" />

###### Photograph Adam Swank - [https://flic.kr/p/72h3qN](https://flic.kr/p/72h3qN) - Creative Commons License CC BY-SA 2.0

By then I was a couple of years drug free (more about that another time), yet I found myself falling into a pigeon hole when meeting new people. I thought I had a great idea, it was based upon pain experienced by many in the fitness industry that I had shared experience. And yet, when people would hear that I was creating a fitness tech business, they would presume it was a lifestyle blog|wearables|just like {insert latest startup fad}.

I needed to change tactics and build recognition through determined action. Otherwise known: rising above the noise of the tech scene bs. #Strategy101.

This particular moment is not unique by any stretch of the imagination. There is an entire industry, heavily influenced by internet marketing, that feasts upon entrepreneurs. It doesn't even wait to wipe its mouth between bites.

I was spending a lot of time researching and soon recognised networkers often were people who:

1) want to get rich quick - at any ethical cost
2) were trapped by fear of failure - rather than trying to refine a failure they might tell me 'Oh I've tried that.. it didn't work.' or even 'I don't think I'm cut out to be an entrepreneur' when their path was not paved with a TechCrunch article and VC capital chaser
3) wanted to know you if you only if you were successful - didn't stick around to hear of the small lessons
4) shared as little as they could

So ever the person to spot what most people are doing I started to do the complete opposite: Take the long road, be proud of my high ethics, learn to absolutely love failure, learn from others failures, speak to the people around the fringes at events, celebrate small successes, share everything I learned. Rather than focus on the biggest trends of the moment I started to look at the underdogs. These tend to be those that understand being humble through life's experiences.

And this was where I came to Mozilla. Ethically, I found their not-for-profit status to be a gift. No only are they the underdogs, with their free and open source Firefox browser, but their [core policies](https://www.mozilla.org/en-US/about/manifesto/) remain driven to enable through working open.

By chance (yay Twitter) I stumbled upon a call for [Mozfest](https://mozillafestival.org/) volunteers. I had no idea how much the experience would alter my life.

Fast forward to 2016. And this is what that simple act of giving back through Mozfest has provided me:

1) a network of activists that are shaping tech for the benefit of all
2) a greater understanding of the very real civil & human rights at risk through using tools we rely upon every single day
3) a community that entices you to share your voice
4) an understanding that the [learning never stops](https://openmatt.org/2016/11/01/fail/)
5) that if you are prepared to engage you will be encouraged
6) a failure is just a stepping stone along your path - there will be more steppingstones ahead of unknown failure rate
7) we have more in common than our differences
8) our differences is what encourages magic to happen
9) working in the open is sometimes messy, but always freeing
10) standards guide you
11) turning things up to 11, and dancing it off, solves many a crisis

Through my activity with them I was fortunate enough to to participate in the Mozilla Foundation's [Open Leadership Training Series](https://github.com/MozillaFoundation/mentorship-program). My path over these 12 weeks of the programme was filled with a lot of failure stepping stones. [Imposter Syndrome](https://www.youtube.com/watch?v=taklO4YIMow) has kept me awake more times than I'd like to admit.

But you know something?

- \*I am not 'failure'
- \*I am actually not a failure - I'd be dead if my body had decided to fail me
- \*Failure is not where I find myself today

I'll leave this 12 week period of self discovery to my next blog post. You won't have to wait long, I have a lot to get off my chest.

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
