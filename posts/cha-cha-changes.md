title: Cha Cha Changes
published_date: "2016-11-22 15:12:00 +0000"
layout: default.liquid
is_draft: false
---
# Cha Cha Changes

Everything changes over time. Even rocky outcrops are worn down by the passing years of winds and torrential rains.

So why do we freak out when things change? Why does it feel so unnatural?

<img src="/blog/img/finetooth.jpg">

By Finetooth (Own work) [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0) or GFDL (http://www.gnu.org/copyleft/fdl.html)], via
Wikimedia Commons from Wikimedia Commons

I have a pet. She was just startled so randomly it nearly gave me a heart attack. Scaredy-cat. Thing is, [cats have truly incredible hearing](https://www.ncbi.nlm.nih.gov/pubmed/4066516). Throughout history people have shown an incessant desire to turn positive things, which we do not entirely understand, into a base level soundbite.

"Oh that's just the way things are around here." Pull into line. [You will respect my authoritaaah!](https://youtu.be/qNYGLdsVU2o)

Through challenges, we are tested. They prime us for the sabre-tooth tiger about to walk into the tribal digs. But could this anxiety, of what terrors might become of us, ultimately hold us back?

In my caring role, some years back, I often read books about anxiety and [control exerted by those they trusted](http://bullyonline.org/). These concerns can certainly prepare you. But should you be preparing for a sabre-tooth tiger in 2017? It was at this time I read a book in which a scientific study observed a long term caged animal. It was a dog, and this study showed the moment from when the cage was opened for the first time.

The dog was free.

And yet it did not leave the cage. Each day they would open the cage and it took longer than expected for the lucky dog to build enough courage to step outside it's opened prison. It sniffed about then, just like my cat, became startled enough to rush back to the safety of the cage.

The scientists tried some gentle encouragement. If I recall correctly it was food they used. Anyway, cutting a long story short, they progressively had to build the confidence in this troubled animal before it had enough of the stuff to finally leave it's cage behind for good. To the dog, the cage was it's home. No matter how free it had now become, there was a clear and deep rooted view held within.

[No matter how bad the circumstances](https://www.psychologytoday.com/blog/the-truisms-wellness/201611/5-faces-toxic-relationships) the cage was the dog's safe place. That, out there, was unknown. Down-right scarier than this safe place. I'll bet that our now released dog would have fought off contenders for that cage. Just as people who are fearful of things [they do not know](http://medicalxpress.com/news/2015-10-brain-energy.html) might lash out and protect their way of living. No matter how much it appears to resemble a cage which they have built around themselves.

Enough of the animal analogies for now. Many of us recognise that fear has a hold of us. We're nearly bludgeoned over the head by it every time we come into contact with media. Marketers psycho-analyse through research groups to further understand how to capture our attention.

How to ramp up the fear, and other entirely natural emotions, to excite and give pleasure rewards. Yeah the reward is often their product, but now we await product launches like we once did a Hollywood blockbuster. Teased to within an inch of it's marketable lifetime we're now treated to fabricated pr releases as 'leaks'. We await changes to the season no longer by the joys of seeing the first snows, but by high budget advertisements which which play on our emotions and make us feel like the world has united us around this 60 second extended play of commerce.

When we no longer have sabre-toothed, life or death, survival instincts to contend with, we can often-times create new experiences, or put greater significance on them than circumstances might dictate. Emotions triggered enough by modern life could blur our senses of instinctive protectionism. Ever been in a heated discussion with someone only to later find out they get a buzz from when they "like a good debate"? You could find yourself feeling a little shirked. "Were they purposely playing with my emotions?"

What I'm trying to express here is that sometimes we have to leave our safe places. Getting out of bed alone is an unbelievable feat of engineering. It's impossible if you're unfortunate enough to be paralised. Paralysis could come in the form of anxiety, but it could equally come in the form of comfort. You worked damn hard for those things you buy to bring a little joy into your family's life. Maybe we could do with the occasional scientist to encourage us with an incremental change to entice us out of our cage?

In recent years I've tried to express myself and express my fears for a reason. This reason will become clearer in my next post. Until then I'll leave you, dear reader, with a little piece of raw emotion.

Turn and face the strange..

<iframe width="560" height="315" src="https://www.youtube.com/embed/xMQ0Ryy01yE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
