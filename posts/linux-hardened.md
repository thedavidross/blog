title: "Linux Hardened"
published_date: "2018-01-03 16:35:01 +0000"
layout: default.liquid
is_draft: false
---
# linux-hardened

Learning things is pretty awesome. I get a great buzz from pushing through personal barriers. But it's of no use if you don't take action on what you've learnt.

This week I got a new (5 years old but new to me) reconditioned Dell e6420 laptop. Here's a [snazzy benchmark comparison](https://www.cpubenchmark.net/compare.php?cmp[]=978&cmp[]=809) of what I upgraded from. So anyway, I've installed an [Arch Linux](https://www.archlinux.org/) encrypted [LVM](https://en.wikipedia.org/wiki/Logical_Volume_Manager_%28Linux%29), again following the steps [I noted](https://linuxinginlondon.wordpress.com/2017/01/17/arch-linux-installation-on-an-encrypted-lvm/) in the [Linuxing in London meetup](https://www.meetup.com/Linuxing-In-London/) blog last year. In the last few days, however, someone brought to my attention that in Arch there is a kernel called 'linux-hardened'. I was intrigued so set about finding more info.

<img src="/blog/img/nails.jpg">

###### 'Nails' by Ed Ivanushkin CC BY-SA 2.0

Now if I'd bothered to read the entire [Arch Wiki entry on Security](https://wiki.archlinux.org/index.php/Security#Kernel_hardening) I'd have likely come across this fact, but hearing it recommended from within the developer community.. well I listen. To some people at least. Sometimes.

Supposedly, it adds improved address space randomization for user space processes, restricts access to kernel logs & pointers, and various further compile time config options.

Switching was a breeze. Here's the very few steps I took:
1. sudo pacman -Syu linux-hardened linux-hardened-headers
2. sudo pacman -R linux linux-headers
3. reboot
4. error: file '/vmlinuz-linux' not found
5. Freak out. Now this step freaked me out a little as it won't automatically locate the kernel, because Grub needs notifying of these changes. I should probably have edited Grub before rebooting, but I didn't. So I'll continue here with how I fixed it. My notes my help someone.
6. mkdir /mnt
7. mount /dev/sda1 /mnt
8. cd /mnt here you'll see all the kernel files
9. cd grub
10. Right, this next step is what I actually did. But it's very very naughty because I could have stuffed it up. It's part of the learning though. If you too are the kind of person that tinkers and breaks things, try to unlearn your bad behaviour. The following is provided for informational purposes only, and to do this *correctly* don't do anything until step 14..
11. nano grub.cfg
12. In this code locate the lines starting with menuentry then within that particular code block you'll discover else ... fi then echo and finally linux with the offending /vmlinuz-linux
13. Edit all references to /vmlinuz-linux, linux-headers (if you've optionally installed them), and linux-fallback-intramfs to the newer /vmlinuz-linux-hardened, linux-hardened-headers, and linux-hardened-fallback-intrafms. NOTE: remember your risk of breaking things is pretty high right now. Consider jumping to the next step without editing anything yet!
14. grub-mkconfig this is a script which will *safely* find all the kernel files you require and far less likely to stuff it all up.
15. reboot

Tadah! Should be all OK now. Will get cracking on reading some more of that Security Wiki.

EDIT: this is how I would do it next if I were to do it again..
1. as above, up to and including its step 2
2. Ctrl + F3 to logout and enter the console
3. login as root
4. grub-mkconfig
5. reboot

As you can see this is *way* less steps to action, and by using the grub-mkconfig script, you'll be using a more rigorous method to implement it.

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
