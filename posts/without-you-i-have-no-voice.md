title: "Without You I Have No Voice"
published_date: "2017-07-25 15:36:07 +0000"
layout: default.liquid
is_draft: false
---
# Without you, I have no voice.

The Open Source Movement has become my inspiration. It offered hope of a way out. It was my lifeline out and it helped show me how to build something out of nothing.

Originally from a banana farm in Australia, top of my class in many subjects I still dropped out of school early, trained as a chef, DJ'd, partied hard, became a store manager, interviewed hundreds, fired a few, made redundant, became a carer, lost everything, trained as a personal trainer, became middle aged unemployable. Life has a habit of forcing you to rebuild.

Then I turned to Open Source and built a website.

Think Ubuntu 7.10 and Wordpress 2.5 in 2008. How do I use LAMP? Solutions to my problems would take me months to solve. Forums would terrify me. 'We don't do things that way', or 'we don't answer such stupid questions'. No mentor, no network, no prior knowledge, I searched, tested, and sweated. I felt I was working beyond my capability. 95% of the time that was correct. I still kept asking those stupid questions anyway, and I learned.

Yet this is for my haters. And I quote "You're an embarrassment", or "I don't see you in tech", or "Why on earth are you even here?"

Before long I had a website (LAMP), self-hosted storage (Owncloud), a blog (Wordpress), a forum (phpBB), a shop (OpenCart), and stock to sell (drop-shipping). Yes it might've taken over a year, and the experiment ultimately failed, but. I. Did. It.

I've now learned Javascript, CSS, HTML, Nodejs, PHP, Python, C, and recently Rust. Contributed to Mozilla, Missing Maps, Wikipedia, and many more projects. I can get by on APIs, encryption, privacy. Read many academic papers and now a big advocate of Open Science. And coffee.

I don't know all the ins and outs, but I could eventually find a solution! I'm immersed in technology. Watched months of MOOCs. Held meetups. Overcame a public speaking fear. Spoken at Google at Bloomerg recently, I'd have NEVER done that a year ago! Volunteered. Given back. If it were not for FOSS I'd have never been able to rebuild, and take my life on an alternative path.

<img src="/blog/img/microphone.jpg">

I've built a network of the right people. They're about as frustrated by the world as I am and we all think: There has to be a better way.

Sometimes there is.

If you've ever helped someone out in Open Source, you'll find that it is YOU who inspires me. By helping others it's helped me get myself out of a hole.

It helped me become more whole.

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
