title: "Alternative Computing"
published_date: "2018-01-04 16:43:01 +0000"
layout: default.liquid
is_draft: false
---
# Alternative Computing

I've been hearing many options thrown about since the US planned dropping of net-neutrality. From uber-fans of [Gopher](https://en.wikipedia.org/wiki/Gopher_%28protocol%29) sites, to mesh networks, to building their own internet. Those with any longer term technical experience summed up many of the alternatives with "Yeah, good luck with that!"

Alongside the UK's own controversial [Snooper's Charter](https://www.theguardian.com/world/2016/nov/29/snoopers-charter-bill-becomes-law-extending-uk-state-surveillance) and wanton ISP wesbite filtering, exemplified via [Open Rights Group](https://www.openrightsgroup.org/)'s efforts seen in [Blocked](https://www.blocked.org.uk/), it can be surprising to see not only the number genuine of small businesses actively unable to be viewed (due to certain scumbag ISPs blocking their websites), but also the level everything is being watched. It's freaky af. Black Mirror freaky.

To kick it all off I decided to reduce the level that ISP has control over me by choosing another DNS. In the past I had switched to Google for this purpose but that was a long time ago now.

To start with I've recently chosen to use [OpenNIC](https://www.opennic.org/)'s DNS. [Setup](https://wiki.opennic.org/) was relatively simple. I downloaded both the wizards for Linux recommended on the OpenNIC website. [nm-opennic](https://github.com/kewlfft/nm-opennic) is pretty cool as it scans for the closest DNS servers to your location, but also alters your resolv.conf file. Mine in Arch Linux would not stay like that though and kept adding in my ISPs namespace. I've had to sudo chattr +i /etc/resolv.conf to get the file stay as I preferred. Even with dhcpcd.service and any additional netctl setups disabled, left over from a recent new install, it otherwise still happened each time I rebooted.

So the second choice is rather dryly called [opennic-resolvconf-update](https://github.com/Fusl/opennic-resolvconf-update). It now more useful due to blocking the previous script being able to edit the file. It's a bit more manual, in that once run you have manually paste in the results into your resolv.conf file, but it's a step foward in my eyes.

This will now use MY choice of [DNS resolving](https://en.wikipedia.org/wiki/Domain_Name_System#DNS_resolvers), and OpenNIC even has their own [TLDs](https://en.wikipedia.org/wiki/Top-level_domain). As far as I could tell this more like a pirate radio of the Web. Instead of resolving via ICAAN approved, often large multinational companies, they have a stack of choices like .geek and [Namecoin](https://namecoin.org/)'s own .bit. Again providing more choices and breaking away from what could be seen as a strangle-hold via US based organisations.

Tried the [BDNS Firefox add-on](https://blockchain-dns.info/). It's certainly redundant with the resolv.conf file now edited, but heck to me at least DNS on the Blockchain made *some* sense. Give it a shot especially if changing your DNS settings is not always possible.

It will come as no surprise that I'm no fan of advertising cookies following my browsing activities. My personal choice is use Firefox's alpha release called Nightly. But I also take further steps in an effort to support the organisations I strongly believe in, and also have improved autonomy in my tech journey.

Also started using [TOR](https://en.wikipedia.org/wiki/Tor_%28anonymity_network%29) a lot more. Mozilla are of course integrating Tor-Browser's core into Firefox via the [Tor Uplift project](https://blog.torproject.org/tor-heart-firefox). I have to say I no longer feel as rebellious in using it thanks to inititiatives like that. Mozilla are taking it even further still in the [Fusion security project](https://wiki.mozilla.org/Security/Fusion). Just as Mozilla is much more than the Firefox browser, Tor is more than the [Tor-Browser](https://www.torproject.org/projects/torbrowser.html.en).

Right inside Arch Linux I discovered [Torsocks](https://www.archlinux.org/packages/community/x86_64/torsocks/) and [Tor](https://www.archlinux.org/packages/community/x86_64/tor/) itself. Torsocks is neat little app that will run your Terminal commands through Tor via torsocks on and of course torsocks off when you're done. I'm now getting all my apps via the Arch repos over Tor. That's just a start and I intend adding further scripts on top of this.

Tor, proper, installs via systemd. I know loads of people hate it, but I don't anything else. The level of interaction I have with it always really simple, and I've found it to be rock solid. To start Tor requires a simple systemctl start tor. Even if you omit the .service part like that it still gets what you mean. You could have it start each to you boot up with systemctl enable tor, but I just start and systemctl stop torwhen I feel I'd like to. Another command to check, just in case, is systemctl status tor. Sometimes the path across all 3 Tor nodes can take some time due to an error or delay. Again it's my desire to have greater granularity in my tech, so these commands might feel a total PITA to many other folks. It's not the only way to use it, just the way I choose to.

I've taken the lead from Tor Fusion and inside Firefox I navigate to about:config and enabled privacy.resistFingerprinting and privacy.firstparty.isolate. There's clearly more to come from this project so stay tuned.

My preferred add-ons selection now include:

- [AdNauseum](https://adnauseam.io/) - based on ublock Origin this fork clicks every link in the background obfuscating data points. No joke - stuff the advertising strenglehold on our daily lives
- [Privacy Settings](https://add0n.com/privacy-settings.html) - adds another stack of setting to tweak
- [Multi-Account Containers](https://github.com/mozilla/multi-account-containers/#readme) - yes this is mostly in Firefox by default, but I love JKT's addition of the keyboard shortcut of Ctrl . which adds a rapid method to access its menu and simply tab on through the available options
- [Containers On The Go](https://addons.mozilla.org/en-US/firefox/addon/containers-on-the-go/) - allows you to quickly add a random one-time use container with Alt c
- [Context Plus](https://addons.mozilla.org/en-US/firefox/addon/context-plus/) - allows you to move stuff into another container. OMG I am forever opening stuff outside a container sofind this one real handy
- [Cookie Auto-Delete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/) - does what it says really
- [Empty Cache Button](https://addons.mozilla.org/en-US/firefox/addon/empty-cache-button/) - I think you're starting to see a them here. This is good when you're tweaking sites yourself too, NOT just removing advertising cookies ;)
- [HTTPS Everywhere](https://www.eff.org/https-everywhere) - well of course.This one's even standard inside Tor-Browser
- [Link Cleaner](https://addons.mozilla.org/en-US/firefox/addon/link-cleaner/) - removes utm parameters. I tend to do this manually anyway, if i see them. This is a handy catch-all just in case I miss some.

Look if you're in advertising, hey, get over it. I still see *some* advertising but only the stuff that respects my privacy. Right now that's certainly not a lot of it. As an industry you can do better than that current privacy obliterating data is the new oxygen business model. Time to sort it out. I hear Google are doing ad blocking. The biggest advertising network on the freaking planet can go take a flying jump thinking I would trust that.

Oh yeah I also switched to the Arch linux-hardened kernel. Early days yet and my install [as described here](https://thedavidross.gitlab.io/blog/posts/linux-hardened.html) was only very recent, but I'm learning more more each day about the kernel. I think a lot of us are after all these recent Intel catastrophic bug announcements like [Meltdown and Spectre](https://meltdownattack.com/).

My next blog entry will be about my recent switch to the distributed social network called [Mastodon](https://joinmastodon.org/). I no longer use Facebook or Twitter. And I feel so much better for it. I even recorded the account deactivation!

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
