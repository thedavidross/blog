title: "150 kids gave me a thumbs up"
published_date: "2017-11-22 16:16:18 +0000"
layout: default.liquid
is_draft: false
---
# 150 kids give me a thumbs up.

On Friday I facilitated one of the sessions at a [Mozfest](https://mozillafestival.org/) Fringe event called, [MozED](https://twitter.com/search?f=tweets&vertical=default&q=%23mozed&src=typd). It was for 150 KS2 students aged 7–11 in London, and it was really great fun.

A month ago I nearly backed out of doing it. I was doing to much in my life and felt close to overload. A piece of advice from a friend soon had me back in check. I'm glad I listened to them.

When I arrived at the venue the organisers had already mostly setup. MozED banners were up all around the place, [Arts Award](http://artsaward.org.uk/) had their table up, the other facilitators were getting the finishing touches done. I had a case of 15 laptops to prepare. [Get all the files](https://github.com/bunnybooboo/MozED) onto each, make sure they had a recent browser. [Firefox Quantum](https://www.mozilla.org/en-US/firefox/) had been released just days before so I took the initiative of installing it on every single one of them. [A-Frame](https://aframe.io/) works on any new browser, but as I was ensuring each one had the updates they required from Microsoft, it seemed right to have at least the tools we were using to be up to date. One had Firefox version 42, which is 2 years old by now.

Don't leave old software on laptops for kids to potentially expose themselves to security risks, folks. The best thing you can do to protect yourself and those around you is to [update all your software](https://advocacy.mozilla.org/en-US/stay-secure/) when updates become available. Every time you put it off you're putting yourself at greater risk.

<blockquote class="twitter-tweet" data-lang="en-gb"><p lang="en" dir="ltr">Discover Arts Award Part C being completed! <a href="https://twitter.com/mozillafestival?ref_src=twsrc%5Etfw">@mozillafestival</a> <a href="https://twitter.com/hashtag/MozEd?src=hash&amp;ref_src=twsrc%5Etfw">#MozEd</a> <a href="https://t.co/AYrUVJfa29">pic.twitter.com/AYrUVJfa29</a></p>&mdash; Arts Award (@ArtsAward) <a href="https://twitter.com/ArtsAward/status/931535115594944512?ref_src=twsrc%5Etfw">17 November 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


Before I'd even had my first coffee all the children and teachers started piling in. I'd never lead workshops for children before and the number of people was a little daunting at first. I chunked it down in my head to the size of each workshop. "Oh yeah! I can deal with that", I said out loud.

I wanted to show them that even with a basic laptop you can be developing for high-end VR equipment that many businesses are already using in production.

"Hands up if you've heard of 'virtual reality'!!" I thought I'd start high energy. The [slide deck](https://bunnybooboo.github.io/MozED/mozed-aframe-slides/index.html) was quite fun.

[The A-Frame workshop](https://github.com/bunnybooboo/MozED) was just a taster. I started with a brief intro to HTML pages: "An HTML file has a 'head' and a 'body', just like you!" Then onto the brains in the head, the Javascript 'script' tag which contains all the hard work from [200+ developers](https://github.com/aframevr/aframe). Then onto the 'body': "where all the action takes place!"

<blockquote class="twitter-tweet" data-lang="en-gb"><p lang="en" dir="ltr">Delighted to welcome 150 young people to <a href="https://twitter.com/hashtag/mozed?src=hash&amp;ref_src=twsrc%5Etfw">#mozed</a> event today. The halls are quiet as everyone plays with <a href="https://twitter.com/hashtag/aframe?src=hash&amp;ref_src=twsrc%5Etfw">#aframe</a> and other cool creative platforms and tech. <a href="https://t.co/4RexoyMUEd">pic.twitter.com/4RexoyMUEd</a></p>&mdash; Mozilla Festival (@mozillafestival) <a href="https://twitter.com/mozillafestival/status/931477516795211776?ref_src=twsrc%5Etfw">17 November 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I was already quite pleased with my analogies, and it was only the first group. [A-Frame](https://aframe.io/) is a [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) library that leaps on top of [Three.js](https://threejs.org/) and brings the advanced technology of [WebGL](https://en.wikipedia.org/wiki/WebGL) in [most VR platforms](https://aframe.io/docs/0.7.0/introduction/vr-headsets-and-webvr-browsers.html) via your everyday Web browser. If you're already familiar with three.js you can bring that into A-Frame and [WebVR](https://webvr.info/) too.

We looked at two boxes and I then had to introduce [Cartesian coordinates](https://en.wikipedia.org/wiki/Cartesian_coordinate_system), the XYZ planes. This is how I did that. "Everybody give me a thumbs up!!". This became a great tool to get everyone's attention throughout the day. "Now point at me", at the front of the room. "Now look at you hand, and what do you see?". The correct answer is of course an 'L'. "'L' for the learning we're doing right now." Here's where some kids were up to speed, and others might have needed a reminder. "The bottom of the 'L' are the 'X' coordinates. They move the box left and right. The top of the 'L' is the 'Y' coordinates. These move the box up and down." My goodness they had it!

"Now if you keep looking at your 'L', point the next finger along at yourself. These are the 'Z' coordinates! And what do the 'Z' coordinates make the box do?"

"Diagonal!!!", "Across!!", "Oh my goodness, my brain!", were just some of the answers. They were so close!

"In and out, or forwards and back!" One kid looked at me and rolled her eyes as if to say 'I knew that! I just chose not to say.' From that moment on I knew I was going to have the best day.

<img src="/blog/img/coordinates.jpg">

Understanding the Cartesian Plane with your hand. Handily (get it?), the way the fingers and thumb point on the right hand are the positive coordinates in A-Frame. Mind. Blown.

Each group was about 30 students, with some actually 2 schools combined. By the end of the end of the day, I'd found the youngest group were the most intensely engaged. The older the group, the more questions I had. The second activity was stacking 2 boxes on top of each other to make "the most rubbish tree you've ever seen!! Who wants to make the most rubbish tree you've ever seen?" Not a single hand stayed down. If we had enough time I showed them how to make a forest.

We ran out of time mostly but the very final group managed to race to the last activity. This time I presented a black coloured plane and introduced 'rotation'. "Sir, sir! What are we doing wrong?!", I was asked when their 3D image suddenly having a black square on the screen. They thought they were doing it wrong, when they'd actually almost advanced to the very last step. The activity then asked them to add another plane so you had a floor and wall. When I showed this with my arms you could tell they were gutted they couldn't finish it.

I then raced to a demo which showed a 3D maze where you could move around the maze with the keyboard arrow keys. Each wall had a presentation or video. The children unreservedly shouted "Oh, WOWWWW!!"

I volunteered to help because I wanted to educate children in some little way, but in turn inspire the teachers. Some of whom I presumed were also parents themselves. I unexpectedly inspired myself. I'd created a really simple workshop that had kids racing each other, leaping out of their seats, peer learning, peer mentoring. I'll treasure the moment when one group were being raced off to their next workshop on their program and to the group I said "Enjoy your next session, it was so great meeting you all". One girl was actually tidying up and she said back to me "And was great meeting you too! You have a great afternoon!"

It floored me. I'd do it all again in a shot. Thanks to Su Adams, from U Can Too, who created the day along with [Esme Simcox](https://twitter.com/EsmeSimcox) who was representing the Mozilla Foundation. Of course big thanks to [Nic Hughes](https://twitter.com/duck_star), Adam, and Neill for their help throughout and support throughout the day too. I had the best time. At least one school have already discussed doing another workshop. No stopping us now!

<blockquote class="twitter-tweet" data-lang="en-gb"><p lang="en" dir="ltr">Thanks to all who came, played and created at <a href="https://twitter.com/hashtag/mozed?src=hash&amp;ref_src=twsrc%5Etfw">#mozed</a> today!  Cannot wait to hear all about your projects in your blogs! <a href="https://t.co/O8SIOhUP2S">pic.twitter.com/O8SIOhUP2S</a></p>&mdash; Mozilla Festival (@mozillafestival) <a href="https://twitter.com/mozillafestival/status/931556286432395265?ref_src=twsrc%5Etfw">17 November 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
