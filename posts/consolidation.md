title: Consolidation
published_date: 2018-11-11 22:00:00 +0000
layout: default.liquid
is_draft: false
---
# Consolidation

### Setting it all up!

Here I am setting up a static page blog in Gitlab. It will look rough for a while, at least until I wrap my head around liquid templates, and the SSG written in Rust, "Cobalt". For now it could use some love and care, so using it regularly seems like one way I can potentially contribute to pull-requests etc.

First up is the intended backing up of all my blogs in Medium and Github. I do not have a big feelings against either platform, to be completely honest. Nor a huge amount of content in either. 

I really like the idea of self-hosting, and I know this is just scratching the surface. I would like to spend more time promoting alternatives though, so this feels like another helpful move to a side of the industry I wish to amplify. FOSS, decentralisation, distribution, alts to GAFA (Google, Apple, Facebook, Amazon), and some down-right, fun explorations.

Learn by doing. See you soon, tech faces!

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
