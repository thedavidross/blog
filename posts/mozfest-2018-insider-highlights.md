title: "mozfest-2018-insider-highlights"
published_date: "2018-11-13 17:24:50 +0000"
layout: default.liquid
is_draft: true
---
# MozFest 2018 Insider Highlights

Coming towards the close of another year already, so it's time I got my thoughts down in this blog. A tremendous amount took place this year. A fantastic number of new connections, and re-invigorating those already on my radar. If I didn't connect with you in 2018, it's simply because I can only do so much.

First cab off the rank is surely being asked to be a Mozilla Festival space wrangler again. Heck 2017 was exhausting on many levels. Between you and me, it really took me until December before I'd recovered from the energy requirements. Really it was a total burn-out. We all know that's not healthy. I was doing a lot that year, and not all of it to the quality I'd expect from myself. By the time the new year began, I'd reluctantly stepped back from several longer term commitments.

This ability to turn some things down, turned out to be a very smart move. It did not at all feel natural to me. This year, the post-event recovery felt like it took barely a whole week. An improvement to my stress levels, on a measurable level like this, will not only have been good for me, but also for those friends around me. I smile a lot. 2018 had a lot more of them.

#### February

It all kicked off with being asked for recommendations for new wranglers in 2018. I did not take this lightly. Wrangling in 2017 was truly an insight. Not only into the working of the Mozilla Foundation, but also to the expectations of the volunteer role. The email actually arrived on the last day of January, but my deliberation took a little while longer.

Gathering together 6 names, each one of made a huge personal impact on my experience of the previous year. 3 women, 2 non-western, 3 technical, 2 educators. I can't go into more details about this but 2 of them became wranglers this year, and 5 of them attended again this year.

This was also the month I got asked to take part in Hack on MDN in Paris. This was to be my first ever supported travel as a contributor to Mozilla. If you use Mozilla's Developer Network's "MDN Web Docs", you'll likely have come across a file I'd messed about at some point. This gathering was primarily for those contributors to the Browser Compatability Tables, that you see towards the bottom of an MDN page. MDN is a part of the Mozilla Corporation. If you're someone that confused by this relationship, then think of the creation of organisations at a time when 4th sector, for-benefit, organisations were not a thing. That makes sense to my brain at least. An older version of a social enterprise. MozFest however, being driven by the Mozilla Foundation, is very much on that social impact axes.

#### March

git call. Yes I know that's not a valid command. Maybe it should be. Anyway, I got the call. The MozFest Production team asked me if I'd like to contribute again. Dear reader, this blog post does not end here. Of course I said yes!

MozFest is an excellent gathering of Mozilla's wider community.

#### April

This is the month where we gathered together at MozRetreat, in Eindhoven, Netherlands. Volunteers and staff get together to brainstorm and design the festival. If you're keeping up it was my 2nd journey to The Continent. For non-Europeans, that basically means to mainland Europe.  


#### May

Global Sprint.

#### June


#### July


#### August


#### September


#### October


#### November

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
