title: "Web-XR London July Meetup"
published_date: "2017-08-26 15:43:44 +0000"
layout: default.liquid
is_draft: false
---
# Web-XR London July Meetup

Last month I held my very first [Mozilla Reps](https://wiki.mozilla.org/ReMo) event and here's a review of what went down.

I was invited to help deliver a WebVR workshop for the [Web XR London Meetup group](https://www.meetup.com/Web-XR/events/241084475/). I would like to emphasise the help part there as it was definitely a group effort, alongside a volunteer [Mozillian](https://www.mozilla.org/en-US/contribute/) in Guillaume Marty, and the organiser Indira. Perfectly timed too as Firefox was just about to include [WebVR by default](https://blog.mozilla.org/blog/2017/08/08/webvr-new-speedy-features/) in its August release.

Indira made some really awesome slides instead of the standard A-Frame deck. It really showed off her coding skills and provided an excellent intro to the workshop. [A-Frame](https://aframe.io/) is great fun to tinker with, and we definitely needed some colour to brighten our day.

There's was a torrential downpour on my walk to [Digital Catapult](https://digital.catapult.org.uk/), our venue for the evening. I had to duck through Westminster Station as it was like a flash flood was about to happen. I arrived at King's Cross pretty wet, but most importantly the cardboards were dry! The rain will have stopped a few people coming to the event. They really missed out.

Those that were brave enough to face the torrent of a 'Great British Summer' were really engaging. Some had barely touched HTML and Javascript before, whilst other had brought their own kit, even a 360 camera. This meant that the workshop that I had [created in Glitch](https://glitch.com/~web-xr-london-a-frame), by reverse engineering an [A-Frame Schools](https://aframe.io/aframe-school/) module, was really helpful.

<img src="/blog/img/2017-07-webxr-meetup.jpg">

Thanks to Petar Savic for agreeing to share this image https://twitter.com/PureSoulMusic/status/890650282794811392

We guided the newbies through the early basics, those that stuck with the workshop some added guidance, to more advanced API testing (hello [audio triggers](https://aframe.io/docs/0.6.0/guides/building-a-basic-scene.html#adding-audio) and the brand new [link component](https://aframe.io/docs/0.6.0/components/link.html#sidebar)), and even some very technical questions surrounding business applications and use cases. Guillaume was a great addition to the team, especially for those more technical questions thrown our way.

There were a few hitches along the way, but for my first official event I think it went well. Several attendees remarked that they could not tell it was my very first attempt at delivering an A-Frame workshop. Always amazes me how internally we have these terrors about how bad we're doing, when in fact we have no idea at all. Their excellent questions, ability to deeply engage, and support of each other was great.

Huge thanks must go to the Mozilla Reps program for giving me a chance to deliver the [Activate workshop](https://activate.mozilla.community/). Through their help I was able to access a small budget and get some Google Cardboards brought in for all attendees. I went for as many as I could and this meant we had some fun with the nonsensical instructions. Fun definitely shaped the evening.

Digital Catapult are an absolutely top notch venue, and their staff could not have been more helpful. I used to work in the hospitality industry so a genuine thanks to that team there. Thanks to Guillaume for sharing his excitement for [WebVR](https://developer.mozilla.org/en-US/docs/Games/Techniques/3D_on_the_web/WebVR) and giving up his valuable free time, and especially to Indira for the offer to do the event at all.

Thanks to Gen Ashley for the use of this photo https://twitter.com/coderinheels/status/890634434034184192Finally a big thanks to Mozilla. So much more than their flagship product, Firefox, their belief in an Open Web and its underlying technologies gives people like us a chance to dive head first into tech. Not only did they provide us with some well deserved pizzas (I told you those who couldn't come because of the rain missed out!), but their belief in open source software and outreaching to the community enables anyone of any skill level to jump right in, contribute, but most of PLAY!

I'll never forget my first Reps event.

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
