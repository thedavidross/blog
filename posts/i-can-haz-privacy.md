title: "I Can Haz Privacy"
published_date: "2016-11-20 13:42:42 +0000"
layout: default.liquid
is_draft: false
---
# I Can Haz Privacy

> No one shall be subjected to arbitrary interference with his privacy, family, home or correspondence, nor to attacks upon his honour and reputation. Everyone has the right to the protection of the law against such interference or attacks.\*\*

###### \*\* Universal Declaration of Human Rights (Article 12)

My experience with the need for privacy is somewhat unusual. Somebody made the decision to go public. Despite that moment being entirely positive, it changed the world around me. I was a somewhat quiet person who kept to themselves. Suddenly my entire community knew exactly who I was. I'd tell you more, but you know, privacy. We had to change telephone number 3 times.

Even good things can affect you in unexpectedly negative ways.

From that moment I became untrusting of people's intentions. An internalised wall went up.

<iframe width="560" height="315" src="https://www.youtube.com/embed/fELRniUbkrM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Wednesday the 16th of November 2016 will do go down in history. The United Kingdom's House of Lords, after a parliamentary 'debate' and 3rd reading, have passed the [Investigative Powers Bill](http://www.wired.co.uk/article/ip-bill-law-details-passed). In a matter of days this will reach Royal Ascension and become UK Law.

The inverted commas over the word debate was entirely intentional. This parliamentary bill [drafted in 2012](https://www.techdirt.com/articles/20120614/14141919329/uk-snoopers-charter-seeks-to-eliminate-pesky-private-communications.shtml) by Theresa May, is also known by it's moniker, the Snooper's Charter. I imagine portraying it as snooping was an attempt to get the general public incensed and take action as early as possible. In July 2015 I had even lobbied my MP as part of a day of action in the Houses of Parliament with [Open Rights Group](https://www.openrightsgroup.org/campaigns/snoopers-charter-lobby-afternoon) and a representative of the Liberal Democrat Party. I learned anyone can do this. You can get scanned through security then ask to speak to your MP. They will ring them, and if unavailable, are required to provide you with a green card which your MP must respond to within 8 days. To have advanced as far as this bill has it past a Joint Committee in [January 2016](https://www.theguardian.com/politics/blog/live/2016/jan/13/cameron-and-corbyn-at-pmqs-politics-live), and [three parliamentary readings](http://services.parliament.uk/bills/2015-16/investigatorypowers/stages.html) since.

I had a conversation on Twitter with a supporter of the Government's official 'Opposition', the Labour Party. 'The Opposition' had felt, along with the SNP, that [abstaining from a vote](http://www.telegraph.co.uk/news/politics/12194441/Snoopers-Charter-Parliamentary-vote-on-the-investigatory-powers-bill-live-updates.html) was the best way to uphold your inalienable human rights. When I pressed this matter on Twitter, I was greeted with a joyous note that a certain Labour MP had inserted "a change of tone." No matter how many ways in which I cut & dice this matter, such a comment shows ignorance and it's not his fault. Politicians don't change things with tone, they change things with voting and representation.

My earlier brush with wall building is sadly not the only experience in which I can bring personal experience. Some years ago I became the victim of a stalker.

A neighbour notified me that someone was waiting outside my door at around 6am. Days later I saw them for myself. I photographed this person in a hire car, hilariously illegally parked, attempting to hide further into the driver's seat from my camera. I was already supporting somebody, who was unfortunately mid-way through a lengthy court battle with this person. I knew exactly who they were.

I approached my local authority to inquire about access to CCTV footage. The same camera system reportedly the [envy of the world](https://www.theguardian.com/uk/2009/mar/02/westminster-cctv-system-privacy). If you like a surveillance society that is. Thinking the police, or court could be rather interested in it's content. I was floored when the request was denied.

If the footage from CCTV cameras is not there to protect London's residents, [WHO ON EARTH IS IT FOR](http://www.bbc.co.uk/news/uk-england-london-36465844)?

This lead me to attend 2 events in the same 7 days. First up was a panel discussion with Mozilla's London [Privacy Lab](https://air.mozilla.org/london-privacy-lab-discussing-the-investigatory-powers-bill/), and next was a screening of [The Haystack](https://www.youtube.com/watch?v=G3-F0yELlz4) with Open Rights Group. (I previously incorrectly stated this screening was at the first event and in researching this article have stumbled upon my error. Apologies to ORG.) I was so engaged by the subject matter I committed on the spot to helping deliver the series more regularly. We held an update panel discussion on the IP & DE bills on the 26th of October 2016. We seem to have had some technical issues in providing this archive but be on the look out for our next event very early in 2017. [Watch this space](https://wiki.mozilla.org/Privacy/Privacy_Lab/London) for more details as we finalise.

So we find ourselves today in the unenviable position of welcoming in [the most extreme surveillance law ever passed in a democracy](http://europe.newsweek.com/ip-bill-most-extreme-surveillance-law-ever-passed-democracy-516573?rm=eu). Take it from me that there were some people working hard to [publicise](https://www.privacyinternational.org/node/725) and take action when it mattered. The LibDems were the only political party I personally saw making any [efforts](http://www.libdemvoice.org/brian-paddick-2-52480.html) towards fighting for our rights. I took some action of my own.

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
