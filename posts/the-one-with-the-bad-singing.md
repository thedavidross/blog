title: "The One With The Bad Singing"
published_date: "2016-11-24 15:29:37 +0000"
layout: default.liquid
is_draft: false
---
# The One With The Bad Singing

What happens when you feel disconnected? Dragged under by what seems this 'regular normal' that is so far from your grasp it feels like you've forgotten how to human.

Discombobulated.

This word exists in this post simply because I like. It makes me laugh. Not knowing anything about its source, I created one for myself. It makes me think of

1. Discos
2. Bob
3. Inflated Balloons

<img src="/blog/img/balloonsdoggo.jpg">

I've never known a Bob. But if I did I have no doubt he'd be the funniest person you'd ever have come across.

Escaping my past required moments of self realisation. One of those moments was realising many of the friends I had surrounded myself with were totally full of shit. They'd outwardly present as 'my body is a temple' living/breathing purity itself. Teaching this pure way of living to other when their own life was a complete disaster.

Om mofo.

I couldn't change them. I wouldn't want to. Nobody can change unless they want to themselves. Tell me I should do something just makes me think of the Beverley Knight song, 'Shoulda Coulda Woulda.. are the last words of a fool.'

So rather than change everyone else under the sun who seemed to frustrate me, I changed myself. This was necessary after feeling so low that I found it hard to concentrate on something longer than 2 minutes. Yes I timed it. So I started the Pomodoro Technique. But it was frustratingly too dull. So to help me get motivated I broke it down even further.

Pomodoro Technique is basically doing some task for 25 minutes. Then taking a break for 5 minutes. Every 3rd turn of doing that you take a 15 minute break. But I was so low that I could not break 5 minutes at a task. I started with 2. I did my 2 minute task and then I rewarded myself. Not with social media or something lame like that. I danced. And sang. And did things gave me goosebumps.

<iframe width="560" height="315" src="https://www.youtube.com/embed/50kP4S0peAs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

It didn't matter how lame it was. If it made me giggle it got in there.

Soon 2 minutes turned to 5. The clothes mountain in my room had been dealt with. 5 minutes turned to 10. I'd had a shave. Break out the 'A-ha'.

It took weeks. And weeks. And weeks. But I did it every day. Until I hit the 25 minutes. If I'd been putting something off, I'd force myself to do it. Not taken the rubbish to the bin? Do it and have a sing.

I realise it sounds really cheesy. But I grew to like the feeling. It made me feel alive. I started to like doing things because of the new way it made me feel connected to myself. Doing something really dumb could soon turn into something fun.

Yeah that rhyme made me feel good too.

Then I discovered I was doing things that were defeating this hard work. I was inwardly fearful of what people thought about me. It really frustrated me and I self-analysed. Why frustration? What made me feel that feeling? It was me that was making myself express that feeling. It is was a bloody stupid waste of time and effort.

So I kept changing.

I found fear in me. So whenever I found that fear, I made a commitment to myself. To do the thing anyway. And then see if the fear still stood.

I was full of shit.

These fears I had held inside me were not mine. They were adapting to hurt. Trying to block out pain and help me not have to face them. Doing this made me not face the things that I enjoyed. So by embracing my fears, and doing them anyway, I found all the frustration and hurt I held inside was me. It was my doing. I had the ability to turn things around. If I felt like I needed a good cry I'd never stop myself. Showers are good for that. Let the water flow. I'd get out of the shower. Wipe the steamy mirror, with my hand, like it was a transition to a new scene in a film. And turn my frown upside down.

Smiling and doing the stupidest laugh I could muster is actually the most incredibly transforming thing I could do. It felt stupid. It looked stupid. In fact it was stupid. So stupid it actually made me laugh. I laughed so hard I could barely breathe.

Like a gust of wind, a feeling could then move, like leaves on the ground by that gust. If frustration got the better of me, I would catch myself out and switch. Switch it from frustration to action. Even something random. Random could be fun. Random was likely to make me laugh. Me laughing makes other people laugh. It doesn't take long to get out of control. I'd stopped trying to control everything and everyone. It felt pretty awesome.

It was around this time that I started pushing myself into the tech world. I can see it came across as a bit crazy, but like I've just said.. I no longer cared what people thought about me.

Walking in the rain.

Hugging a tree.

Skipping.

Eating so much food I can barely move.

Sitting in the British Library. Days on end. Researching and rewarding myself with music.

Saying hi to someone random.

Smiling and leaving a tip.

Singing badly.

Walking. Walking. Walking. Sometimes over 20 miles a day.

I went to a wedding. I'd been drug free a whole year. And this person (who shall remain nameless) called me out. I was a liar. They said. I was an embarrassment. Why was I crying. I SHOULD be over his death. (Should Coulda Woulda).

They would not listen to the hard work I was putting in. This hard work took a long long time. I was hurting and they crushed me. I couldn't say anything. It was all me trying to say what they wanted to hear. They said.

I no longer cared. I knew I was putting in the hard graft. I knew I was drug free. I knew I was feeling inspired for the first time in many many years. I made an active choice to no longer have that person in my life. It made me feel good. If you've been keeping up, you can imagine I might have played some cheesy tune to celebrate. On loop. For 30 minutes.

Whatever it takes.

It really doesn't matter. It doesn't matter if someone hates the song I'm singing. I often hate the song someone else is singing. If you watch them closely though they are having a good time. They're in that moment of playfulness. It's bringing them joy. It might seem like you're enduring finger nails down a chalk board, but please take it from me.

Check. Your. Self.

Let people go through their emotions. Let them be stupid. Let them totally geek the hell out if it's what they want to do. Learn to embrace the difference. Yes even let them destroy their lives, if they so choose to. Because you will not change them. You can not change anyone if they are not willing to themselves. You can not make anyone squeeze the toothpaste out the end of the tube. To leave the toilet seat down. To pick their clothes up. To stop singing that god forsaken song. Again. They will only disappoint you if you keep focusing on this better version of them you have created in your head.

Let them live while they can.

At the beginning of this year I completed something that took me 4 years to complete. I went a whole 21 days of not complaining. If I complained I would go back to day 1 and start all over. On average it takes 6 months to get to 21 days. 3 attempts and 4 years later I actually did it. It's called 'A Complaint Free World'. And, well, it worked for me.

This course of action is not for everyone. I probably should have sought help. But I took control of my own life and became the change I wanted to see. I no longer recognise any fears in me. If I do I have to laugh at myself. 'Yo check yourself.' Yesterday, giving that talk, was a reflection of that.

Turns out yesterday was pretty productive. Nobody really understood the journey I'd taken to get there. I resigned myself to the fact that people have their own shit to deal with. I know I had mine. At least I'd been dealing with it.

Yesterday's team were super productive. 20x more productive than the previous team record. Excuse me while I go and do the most appropriate thing I can do.

I'm going to celebrate.

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
