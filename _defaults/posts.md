layout: default.liquid

title: First Post
is_draft: true
---
# David's blog

Welcome to cobalt.rs!

---
| [Home](/blog/) | [About](/blog/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross) |
