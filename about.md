title: about
layout: default.liquid
---
## About me

Smiley (an oft-used nickname for me). [ENFJ](https://www.16personalities.com/enfj-personality) [LGBTQIA](https://en.wiktionary.org/wiki/LGBTQIA) [FWIW](https://www.urbandictionary.com/define.php?term=FWIW). Keto/LCHF. Certified Personal Fitness Trainer.

Community builder. Open Source advocate. Much more likely to amplify the marginalised, unrecognise, new voices. Keen to improve skills and mentor others in public speaking.

Amplifier of the good in people. Available for hire. Open to free hugs.

---
#### Volunteer activity:
- [Mozilla volunteer](https://mozillians.org/u/david_ross/)
- [MozFest](https://mozillafestival.org/) volunteer since 2014
- [London Privacy Lab](https://wiki.mozilla.org/Privacy/Privacy_Lab/London)
- [Mozilla Reps](https://reps.mozilla.org/u/david_ross/)
- [MDN Web Docs](https://developer.mozilla.org/en-US/profiles/david_ross)
- [Open Leaders](https://foundation.mozilla.org/en/opportunity/mozilla-open-leaders/) mentor
- [Mozilla UK Community](https://discourse.mozilla.org/c/communities/uk)

Previously co-org for the Meetups: Missing Maps, Mastodon London, Tech Speakers London, founding team for Linuxing in London.

---
#### Connect via:
- Email
- [Discourse](https://discourse.mozilla.org/u/david_ross)
- Ricochet
- Slack
- IRC
- Signal
- [Mastodon](https://divad.xyz/@zyx)
- [Matrix](https://matrix.to/#/@david_ross:matrix.org)
- [Wire](@david_ross)

...or over a cuppa or a pint

GPG:
- [0x18e9b7e11b29c1cc](https://gpg.mozilla.org/pks/lookup?search=0x18e9b7e11b29c1cc) (Personal default)
- [0xDFB061EE5C1B8F1B](https://gpg.mozilla.org/pks/lookup?search=0xDFB061EE5C1B8F1B) (ProtonMail specific)

---
#### Currently

Building [FitOpen.org](https://fitopen.org) - an Open Source endurance sports social enterprise. Actively seeking collaborators and partners to bring it to reality. 

---
| [Home](/blog/) | [About](/about.html) | <a rel="me" href="https://divad.xyz/@zyx">Mastodon</a> | [Donate](https://liberapay.com/david_ross/) |

